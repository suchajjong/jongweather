package com.jongzazaal.jongweather

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import com.jongzazaal.jongweather.local.SettingPreference
import com.jongzazaal.jongweather.utility.WeatherUtility
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.component.inject
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import kotlin.test.assertEquals

@Config(sdk = [30])
@RunWith(RobolectricTestRunner::class)
class KoinModulesTest : KoinTest {
//    val context = ApplicationProvider.getApplicationContext<Context>()

    private val weatherUtility by inject<WeatherUtility>()
    private val settingPreference by inject<SettingPreference>()

    @Before
    fun before(){
//        startKoin {
//            androidContext(context)
//            modules(networkModule, sharePreferenceModule, utilityModule)
//
//        }
    }

    @After
    fun after(){
        stopKoin()
    }

    @Test
    fun testKoinModules() {
        assertEquals(weatherUtility.getTemp(12.0, "metric"), "12.00 °C")
        assertEquals(weatherUtility.getTemp(12.0, "imperial"), "12.00 °F")
        assertEquals(weatherUtility.getTemp(0.0, "metric"), "0.00 °C")
        assertEquals(weatherUtility.getTemp(12.5, "metric"), "12.50 °C")
        assertEquals(weatherUtility.getTemp(12.4, "metric"), "12.40 °C")
        assertEquals(weatherUtility.getTemp(12.45, "metric"), "12.45 °C")
        assertEquals(weatherUtility.getTemp(12.456, "metric"), "12.46 °C")

    }

    @Test
    fun testKoinSettingModules() {

        assertEquals(settingPreference.getUnit(), "metric")
        settingPreference.saveUnit("imperial")
        assertEquals(settingPreference.getUnit(), "imperial")

    }
}
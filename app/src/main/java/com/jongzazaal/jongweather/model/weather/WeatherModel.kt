package com.jongzazaal.jongweather.model.weather

import com.jongzazaal.core.base.BaseModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class WeatherModel(
    @field:Json(name = "dt")
    val dt: Long? = null,

    @field:Json(name = "temp")
    val temp: Double? = null,

    @field:Json(name = "humidity")
    val humidity: Double? = null,

    @field:Json(name = "weather")
    val weather: List<WeatherDetailModel>? = null

):BaseModel()

@Parcelize
@JsonClass(generateAdapter = true)
data class WeatherDetailModel(
    @field:Json(name = "id")
    val id: Long? = null,

    @field:Json(name = "main")
    val main: String? = null,

    @field:Json(name = "description")
    val description: String? = null,

    @field:Json(name = "icon")
    val icon: String? = null

):BaseModel()
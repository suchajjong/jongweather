package com.jongzazaal.jongweather.model.weather

import com.jongzazaal.core.base.BaseModel
import com.jongzazaal.core.model.BaseResult
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class WeatherOneCallResponse(

    @field:Json(name = "current")
    val current: WeatherModel? = null,

    @field:Json(name = "hourly")
    val hourly: List<WeatherModel>? = null
):BaseModel()
fun WeatherOneCallResponse.toBaseResult(): BaseResult<WeatherOneCallResponse> {
    return BaseResult<WeatherOneCallResponse>(_data = this)
}
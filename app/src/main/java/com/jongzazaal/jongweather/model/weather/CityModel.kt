package com.jongzazaal.jongweather.model.weather

import com.jongzazaal.core.base.BaseModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class CityModel(
    @field:Json(name = "id")
    val id: Long? = null,

    @field:Json(name = "name")
    val name: String? = null,

    @field:Json(name = "country")
    val country: String? = null,

    @field:Json(name = "coord")
    val coord: LocationModel? = null
): BaseModel()
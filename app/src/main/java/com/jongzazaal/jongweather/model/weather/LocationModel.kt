package com.jongzazaal.jongweather.model.weather

import com.jongzazaal.core.base.BaseModel
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class LocationModel(
    @field:Json(name = "lat")
    val lat: String? = null,

    @field:Json(name = "lon")
    val lon: String? = null
):BaseModel()
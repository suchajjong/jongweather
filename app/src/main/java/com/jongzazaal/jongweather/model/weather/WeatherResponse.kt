package com.jongzazaal.jongweather.model.weather

import com.jongzazaal.core.model.BaseResult
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WeatherResponse(
    @field:Json(name = "coord")
    val location: LocationModel? = null,

    @field:Json(name = "name")
    val name: String? = null,

    @field:Json(name = "cod")
    val cod: String? = null
)

fun WeatherResponse.toBaseResult(): BaseResult<WeatherResponse> {
    return BaseResult<WeatherResponse>(_data = this)
}
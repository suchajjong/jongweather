package com.jongzazaal.jongweather.network

import com.jongzazaal.jongweather.model.weather.WeatherOneCallResponse
import com.jongzazaal.jongweather.model.weather.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceOpenWeatherInterface {
    @GET("/data/2.5/weather")
    suspend fun getCurrentWeather(
        @Query("q") cityName :String,
        @Query("appid") token :String,
        @Query("lang") lang :String = "th",
        @Query("units") units :String = "metric"
    ): WeatherResponse

    @GET("/data/2.5/onecall")
    suspend fun getOneCallWeather(
        @Query("lat") lat :String,
        @Query("lon") lon :String,
        @Query("appid") token :String,
        @Query("lang") lang :String = "th",
        @Query("units") units :String = "metric"
    ): WeatherOneCallResponse
}
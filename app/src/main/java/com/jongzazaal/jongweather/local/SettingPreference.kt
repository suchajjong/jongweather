package com.jongzazaal.jongweather.local

import android.content.Context
import android.content.SharedPreferences
import com.jongzazaal.jongweather.key.WeatherKey

class SettingPreference(var context: Context) {
    companion object {
        private const val PREFERENCE_NAME = "setting_preference"
        private const val KEY_UNIT = "key_unit"

    }

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)


    fun saveUnit(unit: String) {
        sharedPreferences.edit().putString(KEY_UNIT, unit).apply()
    }

    fun getUnit(): String = sharedPreferences.getString(KEY_UNIT, WeatherKey.UNIT_DEFAULT)?:WeatherKey.UNIT_DEFAULT

}
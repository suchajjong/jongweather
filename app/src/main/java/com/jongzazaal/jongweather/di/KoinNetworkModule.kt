package com.jongzazaal.jongweather.di

import com.jongzazaal.core.network.RetrofitManager
import com.jongzazaal.jongweather.module.MainActivityViewModel
import com.jongzazaal.jongweather.repository.OpenWeatherRepository
import com.jongzazaal.jongweather.viewmodel.OpenWeatherViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val networkModule = module {
    single { RetrofitManager }
    factory { OpenWeatherRepository(get()) }

    viewModel<OpenWeatherViewModel>() { OpenWeatherViewModel(androidApplication(), get()) }
    viewModel<MainActivityViewModel>() { MainActivityViewModel(get()) }

}
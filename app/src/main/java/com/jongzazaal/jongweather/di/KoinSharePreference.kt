package com.jongzazaal.jongweather.di

import com.jongzazaal.jongweather.local.SettingPreference
import org.koin.dsl.module

val sharePreferenceModule = module {
    single { SettingPreference(get()) }
}
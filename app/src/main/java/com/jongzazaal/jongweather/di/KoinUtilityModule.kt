package com.jongzazaal.jongweather.di

import com.jongzazaal.jongweather.utility.WeatherUtility
import org.koin.dsl.module

val utilityModule = module {
    single { WeatherUtility(get()) }
}
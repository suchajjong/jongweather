package com.jongzazaal.jongweather.module

import android.os.Bundle
import com.jongzazaal.core.base.BaseActivity
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.jongweather.databinding.ActivityMainBinding
import androidx.lifecycle.Observer
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity(), BaseCommon {
    val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private var presenter: MainActivityPresenter? = null
    private val viewModel: MainActivityViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter = MainActivityPresenter(this)
        binding.presenter = presenter
        binding.viewModel = viewModel
        presenter?.initView()
        presenter?.initListener()
        initViewModel()
        firstCreate()
    }

    override fun firstCreate() {
        viewModel.getCityName()
    }


    override fun initViewModel() {
        viewModel.searchText.observe(this, Observer { result ->
            presenter?.checkFormSearch(result)
        })
        viewModel.cityList.observe(this, Observer { result ->
            presenter?.city = result
            presenter?.refreshList(presenter?.city!!.toMutableList())
        })
    }

}
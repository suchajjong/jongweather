package com.jongzazaal.jongweather.module.weather

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jongzazaal.core.extension.toDateTime
import com.jongzazaal.core.extension.toStringTime
import com.jongzazaal.core.key.JongTimeKey
import com.jongzazaal.jongweather.databinding.AdapterWeatherHoursBinding
import com.jongzazaal.jongweather.key.WeatherKey
import com.jongzazaal.jongweather.local.SettingPreference
import com.jongzazaal.jongweather.model.weather.WeatherModel
import com.jongzazaal.jongweather.utility.WeatherUtility
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class WeatherHoursAdapter(private var data: MutableList<WeatherModel>): RecyclerView.Adapter<WeatherHoursAdapter.ViewHolder>(), KoinComponent {

    private val settingPreference by inject<SettingPreference>()
    private val weatherUtility by inject<WeatherUtility>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = AdapterWeatherHoursBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun refreshList(listItem: List<WeatherModel>){
        this.data = listItem.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: AdapterWeatherHoursBinding): RecyclerView.ViewHolder(binding.root){

        val context = binding.root.context
        fun bind(model: WeatherModel){
            binding.data = WeatherHoursAdapterModel(
                time = model.dt.toDateTime().toStringTime(JongTimeKey.TIME_ONLY),
                temp = weatherUtility.getTemp(model.temp, settingPreference.getUnit()),
                des = model.weather?.first()?.description,
                imgUrl = WeatherKey.IMAGE.format(model.weather?.first()?.icon)
            )
        }

    }
}
data class WeatherHoursAdapterModel(
    var time: String? = null,
    var temp: String? = null,
    var des: String? = null,
    var imgUrl: String? = null
)
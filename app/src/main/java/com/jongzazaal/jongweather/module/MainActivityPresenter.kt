package com.jongzazaal.jongweather.module

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.jongzazaal.core.base.BasePresenter
import com.jongzazaal.core.extension.afterTextChangedCustom
import com.jongzazaal.core.extension.gone
import com.jongzazaal.core.extension.setOnSafeClickListener
import com.jongzazaal.core.extension.visible
import com.jongzazaal.jongweather.model.weather.CityModel
import com.jongzazaal.jongweather.module.setting.SettingActivity
import com.jongzazaal.jongweather.module.weather.WeatherDetailActivity

class MainActivityPresenter(var view: MainActivity): BasePresenter {
    private var adapter: CityAdapter? = null
    lateinit var city: List<CityModel>

    override fun initView() {
        setCityRecycleView()
    }

    override fun initListener() {
        view.binding.searchText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH && !view.binding.searchText.text.isNullOrEmpty()) {
                searchClickListener(view.binding.searchText.text.toString())
                return@OnEditorActionListener true
            }
            return@OnEditorActionListener false
        })
    }

    override fun showLoading() {
        view.binding.loading.visible()
    }

    override fun hideLoading() {
        view.binding.loading.gone()
    }

    private fun setCityRecycleView(){
        val list = arrayListOf<CityModel>()

        if (adapter == null){
            adapter = CityAdapter(list)
            adapter?.setOnItemClickListener { value, position ->
                searchClickListener(value.name?:"")
            }
        }
        view.binding.recycleView.adapter = adapter
    }
    private fun setCitySearch(text: String, city: List<CityModel>){
        val listCityFilter = if (text.isEmpty()){
            city
        }
        else{
            city.filter { text.lowercase() in (it.name?:"").lowercase() }
        }
        adapter?.refreshList(listCityFilter)
    }

    fun refreshList(model: List<CityModel>?){
        adapter?.refreshList(model!!.toMutableList())
    }

    fun settingClickListener(){
        SettingActivity.start(view)
    }
    fun searchClickListener(word:String){
        WeatherDetailActivity.start(view, word)
    }

    fun checkFormSearch(word: String){
        view.binding.submit.isEnabled = word.isNotEmpty()
        setCitySearch(word, city)
    }

}
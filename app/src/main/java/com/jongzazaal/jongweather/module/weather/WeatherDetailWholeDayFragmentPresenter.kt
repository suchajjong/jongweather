package com.jongzazaal.jongweather.module.weather

import com.jongzazaal.core.base.BasePresenter
import com.jongzazaal.jongweather.model.weather.WeatherModel
import com.jongzazaal.jongweather.model.weather.WeatherOneCallResponse

class WeatherDetailWholeDayFragmentPresenter(var view: WeatherDetailWholeDayFragment): BasePresenter {
    private var adapter: WeatherHoursVerticalAdapter? = null
    var model: WeatherOneCallResponse? = null

    override fun initView() {
        setWeatherHouseRecycleView()
    }

    override fun initListener() {}

    override fun showLoading() {}

    override fun hideLoading() {}

    private fun setWeatherHouseRecycleView(){
        val list = arrayListOf<WeatherModel>()

        if (adapter == null){
            adapter = WeatherHoursVerticalAdapter(list)
        }
        view.binding.recycleView.adapter = adapter
    }

    fun refreshList(model: WeatherOneCallResponse?){
        adapter?.refreshList(model?.hourly?: arrayListOf())
    }
}
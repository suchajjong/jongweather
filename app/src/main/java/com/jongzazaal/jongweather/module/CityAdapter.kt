package com.jongzazaal.jongweather.module

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jongzazaal.core.extension.setOnSafeClickListener
import com.jongzazaal.jongweather.databinding.AdapterCityBinding
import com.jongzazaal.jongweather.model.weather.CityModel


class CityAdapter(private var data: MutableList<CityModel>): RecyclerView.Adapter<CityAdapter.ViewHolder>() {

    private var eventItemClickListener: ((value: CityModel, position:Int) -> Unit)? = null
    fun setOnItemClickListener(listener: (value: CityModel, position:Int) -> Unit) {
        eventItemClickListener = listener
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewHolder = AdapterCityBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return ViewHolder(viewHolder)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun refreshList(listItem: List<CityModel>){
        this.data = listItem.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: AdapterCityBinding): RecyclerView.ViewHolder(binding.root){

        val context = binding.root.context
        fun bind(model: CityModel){
            binding.data = model
            binding.layout.setOnSafeClickListener {
                eventItemClickListener?.invoke(model, adapterPosition)
            }
        }

    }
}
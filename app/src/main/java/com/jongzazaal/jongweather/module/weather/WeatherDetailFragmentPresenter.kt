package com.jongzazaal.jongweather.module.weather

import android.view.View
import androidx.navigation.fragment.findNavController
import com.jongzazaal.core.base.BasePresenter
import com.jongzazaal.core.extension.*
import com.jongzazaal.core.key.JongTimeKey
import com.jongzazaal.jongweather.R
import com.jongzazaal.jongweather.key.WeatherKey
import com.jongzazaal.jongweather.local.SettingPreference
import com.jongzazaal.jongweather.model.weather.WeatherModel
import com.jongzazaal.jongweather.model.weather.WeatherOneCallResponse
import com.jongzazaal.jongweather.utility.WeatherUtility
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class WeatherDetailFragmentPresenter(var view: WeatherDetailFragment): BasePresenter, KoinComponent {

    private val settingPreference by inject<SettingPreference>()
    private val weatherUtility by inject<WeatherUtility>()

    private var adapter: WeatherHoursAdapter? = null
    var model: WeatherOneCallResponse? = null

    fun setView(model: WeatherOneCallResponse){
        this.model = model
        adapter?.refreshList(model.hourly?: arrayListOf())
        view.binding.data = WeatherDetailFragmentModel(
            img =  WeatherKey.IMAGE.format(model.current?.weather?.first()?.icon),
            date = model.current?.dt.toDateTime().toStringTime(JongTimeKey.DATE_TIME_OUT_WITH_TIME),
            des = model.current?.weather?.first()?.description,
            humidity = view.binding.root.context.getString(R.string.format_humidity).format(model.current?.humidity),
            temp = weatherUtility.getTemp(model.current?.temp, settingPreference.getUnit()),
            tempDouble = model.current?.temp
        )
    }

    fun setWeatherHouseRecycleView(){
        val list = arrayListOf<WeatherModel>()

        if (adapter == null){
            adapter = WeatherHoursAdapter(list)
        }
        view.binding.recycleView.adapter = adapter
    }

    fun setCityName(name: String){
        view.binding.cityName.text = name
    }

    override fun initView() {
        setWeatherHouseRecycleView()
    }

    override fun initListener() {}

    override fun showLoading() {
        view.binding.loading.visible()
    }

    override fun hideLoading() {
        view.binding.loading.gone()
    }
    fun whoDayButtonClickListener(view: View){
        model?.let { modelResponse->
            val action = WeatherDetailFragmentDirections.actionWeatherDetailFragmentToWeatherDetailWholeDayFragment(modelResponse)
            this.view.findNavController().navigate(action)
        }
    }

}
data class WeatherDetailFragmentModel(
    var cityName: String? = null,
    var img: String? = null,
    var date: String? = null,
    var des: String? = null,
    var humidity: String? = null,
    var temp: String? = null,
    var tempDouble: Double? = null

)
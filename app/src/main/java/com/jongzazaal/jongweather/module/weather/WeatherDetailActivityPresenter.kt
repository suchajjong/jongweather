package com.jongzazaal.jongweather.module.weather

import android.os.Bundle
import androidx.navigation.findNavController
import com.jongzazaal.core.base.BasePresenter
import com.jongzazaal.jongweather.R

class WeatherDetailActivityPresenter(var view: WeatherDetailActivity): BasePresenter {
    var cityName: String? = null

    override fun initView() {
        view.supportActionBar?.title = cityName
        view.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        view.supportActionBar?.setDisplayShowHomeEnabled(true)
        view.findNavController(R.id.nav_weather_fragment).setGraph(
            R.navigation.nav_weather_detail,
            Bundle().apply {
                putString(WeatherDetailFragment.KEY_CITY_NAME, cityName)
            }
        )
    }

    override fun initListener() {}

    override fun showLoading() {}

    override fun hideLoading() {}


}
package com.jongzazaal.jongweather.module.weather

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.core.base.BaseFragment
import com.jongzazaal.jongweather.databinding.FragmentWeatherDetailBinding
import com.jongzazaal.jongweather.extension.showToast
import com.jongzazaal.jongweather.local.SettingPreference
import com.jongzazaal.jongweather.viewmodel.OpenWeatherViewModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * A simple [Fragment] subclass.
 * Use the [WeatherDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WeatherDetailFragment : BaseFragment(), BaseCommon {
    lateinit var binding: FragmentWeatherDetailBinding
    private val viewModel: OpenWeatherViewModel by viewModel()

    private val settingPreference: SettingPreference by inject()

    private lateinit var cityName: String
    private var firstView: Boolean = true

    private var presenter: WeatherDetailFragmentPresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        if (!this::binding.isInitialized) {
            binding = FragmentWeatherDetailBinding.inflate(inflater, container, false)
            presenter = WeatherDetailFragmentPresenter(this)
            binding.presenter = presenter
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            cityName = it.getString(KEY_CITY_NAME, "")
            presenter?.setCityName(cityName)
        }
        if(firstView){
            firstView = false
            presenter?.initView()
            presenter?.initListener()
            initViewModel()
            firstCreate()
        }

    }

    override fun firstCreate() {
        searchWeather(cityName)
    }



    override fun initViewModel() {
        viewModel.weatherOneCallModel.observe(viewLifecycleOwner, Observer { result ->
            presenter?.hideLoading()
            presenter?.setView(result)
            "load success".showToast(requireContext())
        })
        viewModel.error.observe(viewLifecycleOwner, Observer { result ->
            presenter?.hideLoading()
            showErrorDialog(result)
            "load fail ${result.error.name}".showToast(requireContext())
        })
    }
    private fun searchWeather(cityName: String){
        presenter?.showLoading()
        viewModel.getCurrentWeather(cityName, settingPreference.getUnit())
    }

    companion object {
        const val KEY_CITY_NAME = "key_city_name"

        @JvmStatic
        fun newInstance(cityName: String) =
            WeatherDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(KEY_CITY_NAME, cityName)
                }
            }
    }
}
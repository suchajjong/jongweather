package com.jongzazaal.jongweather.module.setting

import com.jongzazaal.core.base.BasePresenter
import com.jongzazaal.jongweather.R
import com.jongzazaal.jongweather.extension.showToast
import com.jongzazaal.jongweather.key.WeatherKey
import com.jongzazaal.jongweather.local.SettingPreference
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SettingActivityPresenter(var view: SettingActivity): BasePresenter, KoinComponent {
    private val settingPreference by inject<SettingPreference>()

    override fun initView() {
        view.supportActionBar?.title = view.getString(R.string.setting)
        view.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        view.supportActionBar?.setDisplayShowHomeEnabled(true)
        setUnit(settingPreference.getUnit())
    }

    override fun initListener() {
        view.binding.radioGroupUnit.setOnCheckedChangeListener { radioGroup, i ->
            when(i){
                R.id.radio_unit_metric -> {
                    settingPreference.saveUnit(WeatherKey.UNIT_METRIC)
                }
                R.id.radio_unit_imperial -> {
                    settingPreference.saveUnit(WeatherKey.UNIT_IMPERIAL)
                }
            }
            "save".showToast(view)
        }
    }

    override fun showLoading() {}

    override fun hideLoading() {}

    private fun setUnit(unit: String){
        when(unit){
            WeatherKey.UNIT_METRIC -> {view.binding.radioGroupUnit.check(R.id.radio_unit_metric)}
            WeatherKey.UNIT_IMPERIAL -> {view.binding.radioGroupUnit.check(R.id.radio_unit_imperial)}
        }
    }
}
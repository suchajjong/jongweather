package com.jongzazaal.jongweather.module

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.jongzazaal.core.extension.getJson
import com.jongzazaal.jongweather.model.weather.CityModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.*

class MainActivityViewModel(app: Application): AndroidViewModel(app) {
    val searchText: MutableLiveData<String> = MutableLiveData()
    val cityList: MutableLiveData<List<CityModel>> = MutableLiveData()

    private val viewModelJob = SupervisorJob()

    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.IO)

    @SuppressLint("CheckResult")
    fun getCityName(){
        viewModelScope.launch {
            val json = "city_thai.json".getJson(getApplication())
            val type = Types.newParameterizedType(List::class.java, CityModel::class.java)
            cityList.postValue(Moshi.Builder().build().adapter<List<CityModel>>(type).fromJson(json))
        }

    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
package com.jongzazaal.jongweather.module.weather

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.jongzazaal.core.base.BaseActivity
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.jongweather.databinding.ActivityWeatherDetailBinding

class WeatherDetailActivity : BaseActivity(), BaseCommon {
    private val binding: ActivityWeatherDetailBinding by lazy { ActivityWeatherDetailBinding.inflate(layoutInflater) }
    private var presenter: WeatherDetailActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter = WeatherDetailActivityPresenter(this)
        presenter?.cityName = intent.getStringExtra(KEY_CITY_NAME)?:"bangkok"
        presenter?.initView()
        presenter?.initListener()
        initViewModel()
        firstCreate()

    }

    override fun firstCreate() {}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initViewModel() {}

    companion object {
        private const val KEY_CITY_NAME = "key_city_name"
        fun start(context: Context, cityName: String) {
            context.startActivity(Intent(context, WeatherDetailActivity::class.java).apply {
                putExtra(KEY_CITY_NAME, cityName)
            })
        }
    }
}
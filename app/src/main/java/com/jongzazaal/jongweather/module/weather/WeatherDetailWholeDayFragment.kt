package com.jongzazaal.jongweather.module.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.core.base.BaseFragment
import com.jongzazaal.jongweather.databinding.FragmentWeatherDetailWholeDayBinding

class WeatherDetailWholeDayFragment : BaseFragment(), BaseCommon {
    lateinit var binding: FragmentWeatherDetailWholeDayBinding

    private var presenter: WeatherDetailWholeDayFragmentPresenter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentWeatherDetailWholeDayBinding.inflate(inflater, container, false)
        presenter = WeatherDetailWholeDayFragmentPresenter(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val args = WeatherDetailWholeDayFragmentArgs.fromBundle(it)
            presenter?.model = args.model
        }
        presenter?.initView()
        presenter?.initListener()
        initViewModel()
        firstCreate()
    }

    override fun firstCreate() {
        presenter?.refreshList(presenter?.model)
    }

    override fun initViewModel() {}



}
package com.jongzazaal.jongweather.module.setting

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.jongzazaal.core.base.BaseActivity
import com.jongzazaal.core.base.BaseCommon
import com.jongzazaal.jongweather.databinding.ActivitySettingBinding

class SettingActivity : BaseActivity(), BaseCommon {
    val binding: ActivitySettingBinding by lazy { ActivitySettingBinding.inflate(layoutInflater) }
    private var presenter: SettingActivityPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        presenter = SettingActivityPresenter(this)
        presenter?.initView()
        presenter?.initListener()
        initViewModel()
        firstCreate()
    }

    override fun firstCreate() {}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initViewModel() {}

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, SettingActivity::class.java).apply {

            })
        }
    }
}
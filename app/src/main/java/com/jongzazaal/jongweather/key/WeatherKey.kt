package com.jongzazaal.jongweather.key

object WeatherKey {
    const val IMAGE = "https://openweathermap.org/img/wn/%s@2x.png"
    const val UNIT_METRIC = "metric"
    const val UNIT_DEFAULT = UNIT_METRIC
    const val UNIT_IMPERIAL = "imperial"

}
package com.jongzazaal.jongweather

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.jongzazaal.jongweather.di.networkModule
import com.jongzazaal.jongweather.di.sharePreferenceModule
import com.jongzazaal.jongweather.di.utilityModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        GlobalContext.startKoin {
            androidLogger(level = org.koin.core.logger.Level.ERROR)
            androidContext(this@MainApplication)
            modules(networkModule, sharePreferenceModule, utilityModule)
        }
    }
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
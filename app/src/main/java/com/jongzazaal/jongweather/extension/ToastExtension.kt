package com.jongzazaal.jongweather.extension

import android.content.Context
import android.widget.Toast
import com.jongzazaal.jongweather.BuildConfig
import org.koin.android.ext.koin.androidApplication

fun Any.showToastDebug(context: Context) {
    if (BuildConfig.IS_PRODUCTION){

    }
    else{
        Toast.makeText(context, "${this}(debug)", Toast.LENGTH_SHORT).show()
    }
}

fun Any.showToast(context: Context) {
    Toast.makeText(context, "${this}", Toast.LENGTH_SHORT).show()
}
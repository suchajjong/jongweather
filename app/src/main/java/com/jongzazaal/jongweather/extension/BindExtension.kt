package com.jongzazaal.jongweather.extension

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.jongzazaal.core.glide.GlideApp
import com.jongzazaal.core.glide.GlidePlaceHolder
import com.jongzazaal.jongweather.R
import com.jongzazaal.jongweather.key.WeatherKey
import com.jongzazaal.jongweather.local.SettingPreference
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@BindingAdapter("imageUrl")
fun ImageView.loadImage(url: String?) {
    GlideApp.with(this.context)
        .load(url)
        .centerCrop()
        .placeholder(GlidePlaceHolder.getPlaceHolder(this.context))
        .error(GlidePlaceHolder.getPlaceHolder(this.context))
        .into(this)
}

@BindingAdapter("textColorTemp")
fun TextView.textColorTemp(temp: Double?) {
    val settingPreference:SettingPreference = getKoinInstance()

    temp?.let {
        if (settingPreference.getUnit() == WeatherKey.UNIT_METRIC){
            val color =
                if (temp < 25.0) { ContextCompat.getColor(this.context, R.color.blue) }
            else if (temp >= 25.0 && temp < 28.0) {ContextCompat.getColor(this.context, R.color.green) }
            else { ContextCompat.getColor(this.context, R.color.red) }
            this.setTextColor(color)
        }
    }


}
inline fun <reified T> getKoinInstance(): T {
    return object : KoinComponent {
        val value: T by inject()
    }.value
}
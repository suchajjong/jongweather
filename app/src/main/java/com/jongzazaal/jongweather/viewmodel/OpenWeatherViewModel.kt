package com.jongzazaal.jongweather.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jongzazaal.core.model.ErrorModel
import com.jongzazaal.jongweather.model.weather.WeatherOneCallResponse
import com.jongzazaal.jongweather.repository.OpenWeatherRepository
import kotlinx.coroutines.*


class OpenWeatherViewModel(app: Application, private var mRepository: OpenWeatherRepository): ViewModel() {
    /**
     * Factory for constructing DevByteViewModel with parameter
     */
//    class Factory(val app: Application, val mRepository: OpenWeatherRepository) : ViewModelProvider.Factory {
//        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//            if (modelClass.isAssignableFrom(OpenWeatherViewModel::class.java)) {
//                @Suppress("UNCHECKED_CAST")
//                return OpenWeatherViewModel(app, mRepository ) as T
//            }
//            throw IllegalArgumentException("Unable to construct viewmodel")
//        }
//    }

    /**
     * This is the job for all coroutines started by this ViewModel.
     *
     * Cancelling this job will cancel all coroutines started by this ViewModel.
     */
    private val viewModelJob = SupervisorJob()

    /**
     * This is the main scope for all coroutines launched by MainViewModel.
     *
     * Since we pass viewModelJob, you can cancel all coroutines launched by uiScope by calling
     * viewModelJob.cancel()
     */
    private val viewModelScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    /**
     * The data source this ViewModel will fetch results from.
     */



    /**
     * A playlist of videos that can be shown on the screen. Views should use this to get access
     * to the data.
     */

    val weatherOneCallModel: LiveData<WeatherOneCallResponse>
        get() = mRepository.weatherOneCallModel

    val error: LiveData<ErrorModel>
        get() = mRepository.error

    /**
     * Refresh data from the repository. Use a coroutine launch to run in a
     * background thread.
     */
    fun getCurrentWeather(cityName: String, unit:String) {
        viewModelScope.launch {
            mRepository.getCurrentWeather(cityName, unit)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}
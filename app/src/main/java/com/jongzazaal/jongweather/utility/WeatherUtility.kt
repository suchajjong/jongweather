package com.jongzazaal.jongweather.utility

import android.content.Context
import com.jongzazaal.core.extension.toNumberComma2Digit
import com.jongzazaal.jongweather.R
import com.jongzazaal.jongweather.key.WeatherKey

class WeatherUtility(val context: Context){

    fun getTemp(temp: Double?, unit: String): String{
        val unit = when(unit){
            WeatherKey.UNIT_METRIC -> {context.getString(R.string.celsius)}
            WeatherKey.UNIT_IMPERIAL -> {context.getString(R.string.fahrenheit)}
            else -> {""}
        }
        return "${temp.toString().toNumberComma2Digit()} $unit"
    }
}
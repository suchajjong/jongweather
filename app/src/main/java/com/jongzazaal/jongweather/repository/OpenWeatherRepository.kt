package com.jongzazaal.jongweather.repository

import androidx.lifecycle.MutableLiveData
import com.jongzazaal.core.model.Error
import com.jongzazaal.core.model.ErrorModel
import com.jongzazaal.core.network.APICallHandler
import com.jongzazaal.core.network.RetrofitManager
import com.jongzazaal.jongweather.BuildConfig
import com.jongzazaal.jongweather.model.weather.WeatherOneCallResponse
import com.jongzazaal.jongweather.model.weather.toBaseResult
import com.jongzazaal.jongweather.network.ServiceOpenWeatherInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OpenWeatherRepository(val retrofit: RetrofitManager) {

    val weatherOneCallModel: MutableLiveData<WeatherOneCallResponse> = MutableLiveData()
    val error: MutableLiveData<ErrorModel> = MutableLiveData()

    suspend fun getCurrentWeather(cityName: String, unit:String) {
        withContext(Dispatchers.IO) {
            val model = APICallHandler.handlerMessage{
                RetrofitManager.setBaseUrl(BuildConfig.BASE_URL)
                    .create(ServiceOpenWeatherInterface::class.java)
                    .getCurrentWeather(
                        cityName = cityName,
                        token = BuildConfig.OPEN_WEATHER_TOKEN,
                        units = unit)
                    .toBaseResult()}
            if (model.error.error == Error.SUCCESS){
                getOneCallWeather(model.data?.location?.lat?:"", model.data?.location?.lon?:"", unit)
            }else{
                error.postValue(model.error)
            }

        }
    }

    suspend fun getOneCallWeather(lat: String, lon: String, unit:String) {
        withContext(Dispatchers.IO) {
            val model = APICallHandler.handlerMessage{
                RetrofitManager.setBaseUrl(BuildConfig.BASE_URL)
                    .create(ServiceOpenWeatherInterface::class.java)
                    .getOneCallWeather(
                        lat = lat, lon = lon,
                        token = BuildConfig.OPEN_WEATHER_TOKEN,
                        units = unit)
                    .toBaseResult()}
            if (model.error.error == Error.SUCCESS){
                weatherOneCallModel.postValue(model.data)
            }else{
                error.postValue(model.error)
            }

        }
    }
}
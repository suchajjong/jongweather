package com.jongzazaal.core.key

object JongTimeKey {
    const val DATE_TIME_IN = "yyyy-MM-dd"
    const val DATE_TIME_IN_WITH_TIME = "yyyy-MM-dd HH:mm:ss"
    const val DATE_TIME_OUT = "dd MMM yy"
    const val DATE_TIME_OUT_2 = "dd MMM yyyy"
    const val DATE_TIME_OUT_3 = "dd MMMM yyyy"
    const val TIME_ONLY = "HH:mm"
    const val DATE_TIME_OUT_4 = "dd/MM/yyyy"
    const val DATE_TIME_OUT_5 = "dd-MM-yyyy"
    const val DATE_TIME_OUT_WITH_TIME = "dd-MM-yyyy HH:mm"
}
package com.jongzazaal.core.extension

import java.text.DecimalFormat

fun String?.toNumberComma(): String {
    if (this.isNullOrEmpty()|| this == "null"){
        return "0"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,##0")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}
fun String?.toNumberComma2Digit(): String {
    if (this.isNullOrEmpty() || this == "null"){
        return "0.00"
    }
    val str = this.replace(",", "")

    var formatter = DecimalFormat("###,###,###,##0.00")


    formatter.negativePrefix = "- "
    return formatter.format(str.toFloat())
}

fun String?.toNumberComma2DigitAuto(): String {
    if (this.isNullOrEmpty() || this == "null"){
        return "0"
    }
    val str = this.replace(",", "")

    var formatter = DecimalFormat("###,###,###,##0.00")


    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString.replace(".00","")
}

fun String?.toNumberComma1Digit(): String {
    if (this.isNullOrEmpty()|| this == "null"){
        return "0"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,##0.0")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}
fun String?.toNumberComma1DigitAuto(): String {
    if (this.isNullOrEmpty() || this == "null"){
        return "0"
    }
    val str = this.replace(",", "")

    var formatter = DecimalFormat("###,###,###,##0.0")


    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString.replace(".0","")
}

fun String?.toNumberCommaAuto(): String {
    if (this.isNullOrEmpty()|| this == "null"){
        return "0"
    }
    val str = this.replace(",", "")
    val formatter = DecimalFormat("###,###,###,##0.##")
    formatter.negativePrefix = "- "
    val yourFormattedString = formatter.format(str.toFloat())
    return yourFormattedString
}
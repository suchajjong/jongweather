package com.jongzazaal.core.extension

import com.jongzazaal.core.key.JongTimeKey
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.chrono.ThaiBuddhistChronology
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.temporal.UnsupportedTemporalTypeException
import java.text.SimpleDateFormat
import java.util.*

fun String?.toStringTime(formatIn:String = JongTimeKey.DATE_TIME_IN_WITH_TIME, formatOut:String): String{
    if (this.isNullOrEmpty()){return ""}

    try {
        val formatter = DateTimeFormatter.ofPattern(formatIn)
        val localDate = LocalDate.parse(this, formatter)
        val toThai: DateTimeFormatter = DateTimeFormatter.ofPattern(formatOut)
            .withChronology(ThaiBuddhistChronology.INSTANCE)
            .withLocale(Locale("th"))
        return toThai.format(localDate)
    }
    catch (e: UnsupportedTemporalTypeException){
        val formatter = DateTimeFormatter.ofPattern(formatIn)
        val localDate = LocalDateTime.parse(this, formatter)
        val toThai: DateTimeFormatter = DateTimeFormatter.ofPattern(formatOut)
            .withChronology(ThaiBuddhistChronology.INSTANCE)
            .withLocale(Locale("th"))
        return toThai.format(localDate)
    }

}

fun String?.toDateTime(): Date{
    if (this.isNullOrEmpty()){return Date()}
    return Date(this.toDouble().toLong()*1000)

}
fun Double?.toDateTime(): Date{
    if (this == null || this == 0.0){return Date()}
    return Date(this.toDouble().toLong()*1000)

}

fun Long?.toDateTime(): Date{
    if (this == null || this == 0L){return Date()}
    return Date(this*1000)

}

fun Date.toStringTime(formatOut:String): String{

    val formatter = SimpleDateFormat(JongTimeKey.DATE_TIME_IN_WITH_TIME, Locale("th"))
    val dateString = formatter.format(this)
    return dateString.toStringTime(JongTimeKey.DATE_TIME_IN_WITH_TIME, formatOut)

}
package com.jongzazaal.core.extension

import android.content.Context
import java.io.IOException

fun String.getJson(context: Context): String?{
    val jsonString: String
    try {
        //filename.json
        jsonString = context.assets.open(this).bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return jsonString
}
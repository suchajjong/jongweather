package com.jongzazaal.core.base

import android.widget.Toast
import androidx.fragment.app.Fragment
import com.jongzazaal.core.model.ErrorModel
import com.jongzazaal.customview.CustomMainDialog

open class BaseFragment: Fragment() {
    protected fun showErrorDialog(errorModel: ErrorModel){
        val errorMessage = if (errorModel.msg.isNotEmpty()){errorModel.msg}else{
            errorModel.error.name
        }
        if (!childFragmentManager.isDestroyed){
            if (errorModel.msgFromServer.isNotEmpty() || errorModel.codeFromServer != null){
                val dialog = CustomMainDialog.newInstance(
                    title = errorModel.msgFromServer,
                    des = (if (errorModel.codeFromServer == null){""}else{"(error ${errorModel.codeFromServer})"}),
                    centerButton = "ตกลง"
                )
                dialog.setOnCenterClickListener {
                    dialog.dismissAllowingStateLoss()
                }

                try {
                    dialog.show(childFragmentManager, "tag_error")
                }
                catch (e: Exception){
                    Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                    Toast.makeText(requireContext(), errorModel.msgFromServer, Toast.LENGTH_SHORT).show()
                }
            }
            else{
                val des = if (errorModel.code.isNotEmpty()){"(${errorModel.code})"}else{""}
                val dialog = CustomMainDialog.newInstance(
                    title = errorMessage,
                    des = des,
                    centerButton = "ตกลง"
                )
                dialog.setOnCenterClickListener {
                    dialog.dismissAllowingStateLoss()
                }
                try {
                    dialog.show(childFragmentManager, "tag_error")
                }
                catch (e: Exception){
                    Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
                    if (!errorModel.msgFromServer.isEmpty()){
                        Toast.makeText(requireContext(), errorModel.msgFromServer, Toast.LENGTH_SHORT).show()
                    }
                }
            }


        }

    }
}
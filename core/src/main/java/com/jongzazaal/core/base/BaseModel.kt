package com.jongzazaal.core.base

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
open class BaseModel(
    @field:Json(name = "app")
    val app: String = "jong"
): Parcelable
package com.jongzazaal.core.base

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.jongzazaal.core.model.ErrorModel
import com.jongzazaal.customview.CustomMainDialog

open class BaseActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    protected fun showErrorDialog(errorModel: ErrorModel){
        val errorMessage = if (errorModel.msg.isNotEmpty()){errorModel.msg}else{
            errorModel.error.name
        }
        if (!supportFragmentManager.isDestroyed){
            if (errorModel.msgFromServer.isNotEmpty() || errorModel.codeFromServer != null){
                    val dialog =CustomMainDialog.newInstance(
                        title = errorModel.msgFromServer,
                        des = (if (errorModel.codeFromServer == null){""}else{"(error ${errorModel.codeFromServer})"}),
                        centerButton = "ตกลง"
                        )
                dialog.setOnCenterClickListener {
                    dialog.dismissAllowingStateLoss()
                }

                try {
                    dialog.show(supportFragmentManager, "tag_error")
                }
                catch (e: Exception){
                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
                    Toast.makeText(this, errorModel.msgFromServer, Toast.LENGTH_SHORT).show()
                }
            }
            else{
                val des = if (errorModel.code.isNotEmpty()){"(${errorModel.code})"}else{""}
                val dialog =CustomMainDialog.newInstance(
                    title = errorMessage,
                    des = des,
                    centerButton = "ตกลง"
                )
                dialog.setOnCenterClickListener {
                    dialog.dismissAllowingStateLoss()
                }
                try {
                    dialog.show(supportFragmentManager, "tag_error")
                }
                catch (e: Exception){
                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
                    if (!errorModel.msgFromServer.isEmpty()){
                        Toast.makeText(this, errorModel.msgFromServer, Toast.LENGTH_SHORT).show()
                    }
                }
            }


        }

    }
}
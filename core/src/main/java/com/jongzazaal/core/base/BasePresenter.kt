package com.jongzazaal.core.base

interface BasePresenter {
    fun initView()
    fun initListener()
    fun showLoading()
    fun hideLoading()
}
package com.jongzazaal.core.base

interface BaseCommon {
    fun firstCreate()
    fun initViewModel()
}
package com.jongzazaal.core.network

import com.jongzazaal.core.model.BaseResult
import com.jongzazaal.core.model.BaseResultList
import com.jongzazaal.core.model.ErrorModel
import com.jongzazaal.core.model.Error
import com.squareup.moshi.JsonDataException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object APICallHandler {

    suspend inline fun<T> handlerMessage(
        crossinline response: suspend () -> BaseResult<T>
    ): BaseResult<T> {
        return try {
            withContext(Dispatchers.IO){
                with(response()){
                    return@with this
                }
            }
        }catch (e: Exception){
            withContext(Dispatchers.IO){
                val err = when(e){
                    is HttpException -> {
                        Error.HTTP_EXCEPTION
                    }
                    is SocketTimeoutException -> Error.TIMEOUT
                    is JsonDataException -> Error.JSON_PAIR_FAIL
                    is UnknownHostException -> Error.NO_INTERNET_CONNECTION
                    else -> {
                        Error.OTHER
                    }

                }
                val errCode = when(e){
                    is HttpException -> {e.code().toString()}
                    else -> {""}
                }
                val msgFromServer = when(e){
                    is HttpException -> {
                        try {
                            val json = JSONObject(e.response()?.errorBody()?.string())
                            json.getString("message")
                        }
                        catch (e: java.lang.Exception){
                            ""
                        }
                    }
                    else -> {""}
                }
                val codeFromServer = when(e){
                    is HttpException -> {
                        try {
                            val json = JSONObject(e.response()?.errorBody()?.string())
                            json.getInt("cod")
                        }
                        catch (e: java.lang.Exception){
                            null
                        }
                    }
                    else -> {null}
                }
                e.printStackTrace()
                return@withContext BaseResult<T>(
                    _error = ErrorModel(
                        error = err,
                        code = errCode,
                        msgFromServer = msgFromServer,
                        codeFromServer = codeFromServer
                    )
                )
            }
        }
    }
    suspend inline fun<T> handlerMessageList(
        crossinline response: suspend () -> BaseResultList<T>
    ): BaseResultList<T> {
        return try {
            withContext(Dispatchers.IO){
                with(response()){
                    return@with this
                }
            }
        }catch (e: Exception){
            withContext(Dispatchers.IO){
                val err = when(e){
                    is HttpException -> {
                        Error.HTTP_EXCEPTION
                    }
                    is SocketTimeoutException -> Error.TIMEOUT
                    is JsonDataException -> Error.JSON_PAIR_FAIL
                    is UnknownHostException -> Error.NO_INTERNET_CONNECTION
                    else -> {
                        Error.OTHER
                    }

                }
                val msgFromServer = when(e){
                    is HttpException -> {
                        try {
                            val json = JSONObject(e.response()?.errorBody()?.string())
                            json.getString("message")
                        }
                        catch (e: java.lang.Exception){
                            ""
                        }
                    }
                    else -> {""}
                }
                val codeFromServer = when(e){
                    is HttpException -> {
                        try {
                            val json = JSONObject(e.response()?.errorBody()?.string())
                            json.getInt("cod")
                        }
                        catch (e: java.lang.Exception){
                            null
                        }
                    }
                    else -> {null}
                }
                e.printStackTrace()
                return@withContext BaseResultList<T>(
                    _error = ErrorModel(
                        error = err,
                        msgFromServer = msgFromServer,
                        codeFromServer = codeFromServer
                    )
                )
            }
        }
    }

    fun handlerMessageRx(e: Throwable): ErrorModel{
        val err = when(e){
            is HttpException -> {
                Error.HTTP_EXCEPTION
            }
            is SocketTimeoutException -> Error.TIMEOUT
            is JsonDataException -> Error.JSON_PAIR_FAIL
            is UnknownHostException -> Error.NO_INTERNET_CONNECTION
            else -> {
                Error.OTHER
            }

        }
        val errCode = when(e){
            is HttpException -> {e.code().toString()}
            else -> {""}
        }
        val msgFromServer = when(e){
            is HttpException -> {
                try {
                    val json = JSONObject(e.response()?.errorBody()?.string())
                    json.getString("message")
                }
                catch (e: java.lang.Exception){
                    ""
                }
            }
            else -> {""}
        }
        val codeFromServer = when(e){
            is HttpException -> {
                try {
                    val json = JSONObject(e.response()?.errorBody()?.string())
                    json.getInt("cod")
                }
                catch (e: java.lang.Exception){
                    null
                }
            }
            else -> {null}
        }
        e.printStackTrace()
        return ErrorModel(
            error = err,
            code = errCode,
            msgFromServer = msgFromServer,
            codeFromServer = codeFromServer
        )

    }
}
package com.jongzazaal.core.glide

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.jongzazaal.core.R

object GlidePlaceHolder {
    fun getPlaceHolder(context: Context, colorRes: Int = R.color.red): CircularProgressDrawable {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(context, colorRes))
        circularProgressDrawable.start()
        return circularProgressDrawable
    }

}
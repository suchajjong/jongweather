package com.jongzazaal.core.glide

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

@GlideModule
class JongGlideModule: AppGlideModule() {
    override fun applyOptions(context: Context, builder: GlideBuilder) {

        builder.setDefaultRequestOptions(
            RequestOptions()
                .format(DecodeFormat.PREFER_RGB_565)
                .skipMemoryCache(true)
        )

    }
//    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
//        val okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient()
//        registry.replace(GlideUrl::class.java, InputStream::class.java, OkHttpUrlLoader.Factory(okHttpClient))
//    }
}
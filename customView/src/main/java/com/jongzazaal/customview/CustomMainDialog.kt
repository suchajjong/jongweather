package com.jongzazaal.customview

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.appcompat.app.AppCompatDialogFragment
import com.jongzazaal.customview.databinding.DialogCustomMainBinding

class CustomMainDialog: AppCompatDialogFragment() {
    private lateinit var binding: DialogCustomMainBinding
    private var eventCenterClickListener: (() -> Unit)? = null
    fun setOnCenterClickListener(listener: () -> Unit) {
        eventCenterClickListener = listener
    }
    private var eventLeftClickListener: (() -> Unit)? = null
    fun setOnLeftClickListener(listener: () -> Unit) {
        eventLeftClickListener = listener
    }
    private var eventRightClickListener: (() -> Unit)? = null
    fun setOnRightClickListener(listener: () -> Unit) {
        eventRightClickListener = listener
    }
    private var eventAutoDismissListener: (() -> Unit)? = null
    fun setOnAutoDismissListener(listener: () -> Unit) {
        eventAutoDismissListener = listener
    }
    private var eventOnDismissListener: (() -> Unit)? = null
    fun setOnDismissListener(listener: () -> Unit) {
        eventOnDismissListener = listener
    }

    private var icon = ""
    private var title = ""
    private var des = ""
    private var iconColorRes = R.color.error_main
    private var centerButton = ""
    private var leftButton = ""
    private var rightButton = ""
    private var autoDismiss = -1L


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.setGravity(Gravity.CENTER)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        isCancelable = true

        return dialog
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DialogCustomMainBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            restoreArguments(requireArguments())
        } else {
            restoreInstanceState(savedInstanceState)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initInstance()

    }

    @SuppressLint("SetTextI18n")
    private fun initInstance() {
        binding.title.text = title
        binding.des.text = des
        binding.centerButton.text = centerButton
        binding.leftButton.text = leftButton
        binding.rightButton.text = rightButton

        checkEmpty(icon, binding.icon)
        checkEmpty(title, binding.title)
        checkEmpty(des, binding.des)
        checkEmpty(centerButton, binding.centerButton)
        checkEmpty(leftButton, binding.layoutLeftLightButton)

        binding.centerButton.setOnClickListener {
            eventCenterClickListener?.invoke()
        }
        binding.leftButton.setOnClickListener {
            eventLeftClickListener?.invoke()
        }
        binding.rightButton.setOnClickListener {
            eventRightClickListener?.invoke()
        }

        if (autoDismiss != -1L){
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed(Runnable {
                if (dialog != null){
                    dismissAllowingStateLoss()
                }
                eventAutoDismissListener?.invoke()
            }, autoDismiss)
        }

    }

    override fun onResume() {
        super.onResume()
        val width = (resources.displayMetrics.widthPixels * 0.8).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(KEY_ICON, icon)
        outState.putString(KEY_TITLE, title)
        outState.putString(KEY_DES, des)
        outState.putInt(KEY_ICON_COLOR, iconColorRes)
        outState.putString(KEY_CENTER_BUTTON, centerButton)
        outState.putString(KEY_LEFT_BUTTON, leftButton)
        outState.putString(KEY_RIGHT_BUTTON, rightButton)
        outState.putLong(KEY_AUTO_DISMISS, autoDismiss)
    }

    private fun restoreInstanceState(bundle: Bundle) {
        icon = bundle.getString(KEY_ICON, "")
        title = bundle.getString(KEY_TITLE, "")
        des = bundle.getString(KEY_DES, "")
        iconColorRes = bundle.getInt(KEY_ICON_COLOR)
        centerButton = bundle.getString(KEY_CENTER_BUTTON, "")
        leftButton = bundle.getString(KEY_LEFT_BUTTON, "")
        rightButton = bundle.getString(KEY_RIGHT_BUTTON, "")
        autoDismiss = bundle.getLong(KEY_AUTO_DISMISS, -1L)

    }

    private fun restoreArguments(bundle: Bundle) {
        icon = bundle.getString(KEY_ICON, "")
        title = bundle.getString(KEY_TITLE, "")
        des = bundle.getString(KEY_DES, "")
        iconColorRes = bundle.getInt(KEY_ICON_COLOR)
        centerButton = bundle.getString(KEY_CENTER_BUTTON, "")
        leftButton = bundle.getString(KEY_LEFT_BUTTON, "")
        rightButton = bundle.getString(KEY_RIGHT_BUTTON, "")
        autoDismiss = bundle.getLong(KEY_AUTO_DISMISS, -1L)

    }

    private fun checkEmpty(text: String, view: View){
        if (text.isEmpty()){
            view.visibility = View.GONE
        }
        else{
            view.visibility = View.VISIBLE
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        eventOnDismissListener?.invoke()
        super.onDismiss(dialog)

    }
    companion object {
        private const val TAG = "DialogMain"
        private const val KEY_ICON = "key_icon"
        private const val KEY_TITLE = "key_title"
        private const val KEY_DES = "key_des"
        private const val KEY_ICON_COLOR = "key_ico_color"

        private const val KEY_CENTER_BUTTON = "key_center_button"
        private const val KEY_LEFT_BUTTON = "key_left_button"
        private const val KEY_RIGHT_BUTTON = "key_right_button"

        private const val KEY_AUTO_DISMISS = "key_auto_dismiss"

        fun newInstance(title:String = "", des: String = "", centerButton: String = "", autoDismiss: Long = -1): CustomMainDialog{
            return CustomMainDialog().apply {
                arguments = Bundle().apply {
                    putString(KEY_ICON, icon)
                    putString(KEY_TITLE, title)
                    putString(KEY_DES, des)
                    putInt(KEY_ICON_COLOR, iconColorRes?:R.color.error_main)
                    putString(KEY_CENTER_BUTTON, centerButton)
                    putLong(KEY_AUTO_DISMISS, autoDismiss)

                }
            }
        }
        fun newInstance(title:String = "", des: String = "", leftButton: String = "", rightButton: String = "", autoDismiss: Long = -1): CustomMainDialog{
            return CustomMainDialog().apply {
                arguments = Bundle().apply {
                    putString(KEY_ICON, icon)
                    putString(KEY_TITLE, title)
                    putString(KEY_DES, des)
                    putInt(KEY_ICON_COLOR, iconColorRes?:R.color.error_main)
                    putString(KEY_LEFT_BUTTON, leftButton)
                    putString(KEY_RIGHT_BUTTON, rightButton)
                    putLong(KEY_AUTO_DISMISS, autoDismiss)

                }
            }
        }
    }
}
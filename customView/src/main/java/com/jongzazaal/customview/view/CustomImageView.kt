package com.jongzazaal.customview.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class CustomImageView: AppCompatImageView {

    constructor(context: Context):
            super(context)
    constructor(context: Context, attrSet: AttributeSet):
            super(context, attrSet)
    constructor(context: Context, attrSet: AttributeSet, defStyleInt: Int):
            super(context, attrSet, defStyleInt)

}
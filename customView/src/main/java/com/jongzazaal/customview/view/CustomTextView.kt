package com.jongzazaal.customview.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class CustomTextView: AppCompatTextView {

    private var paint: Paint? = null
    private var strike = false
    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet,
        defStyleAttr: Int = android.R.attr.textViewStyle)
            : super(context, attrs, defStyleAttr) {
        initAttrs(context, attrs)
    }
    private fun initAttrs(context: Context, attrs: AttributeSet) {
        paint = Paint()
        paint!!.color = currentTextColor
        paint!!.strokeWidth = resources.displayMetrics.density * 1
    }
    fun addStrike(){
        strike = true
        invalidate()
    }
    fun removeStrike(){
        strike = false
        invalidate()
    }

    fun setGradientTextview(): LinearGradient {
        val paint = this.getPaint()
        val width = paint.measureText("Tianjin, China")

        val textShader = LinearGradient(
            0f, 0f, width, this.getTextSize(),
            intArrayOf(
                Color.parseColor("#F97C3C"),
                Color.parseColor("#FDB54E"),
                Color.parseColor("#64B678"),
                Color.parseColor("#478AEA"),
                Color.parseColor("#8446CC")
            ), null, Shader.TileMode.CLAMP
        )
        return textShader

    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        paint!!.color = currentTextColor
        paint!!.strokeWidth = textSize/6f
        if (strike) {
            canvas?.drawLine(0f, height / 2f, width.toFloat(),
                height / 2f, paint?:this.getPaint());
        }
    }

}
package com.jongzazaal.customview.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.jongzazaal.customview.R

class CustomButton: AppCompatButton {

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet,
        defStyleAttr: Int = R.attr.buttonStyle)
            : super(context, attrs, defStyleAttr) {
        initAttrs(context, attrs)
    }
    private fun initAttrs(context: Context, attrs: AttributeSet) {

    }
}